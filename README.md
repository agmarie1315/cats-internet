# Cats Internet

## Description of project
There is a saying about the Internet being about cats. There is a belief that cat's are soothing and people share cats to improve their well being. In this project, we propose to measure the amount of content about cats that is shared in social media. 

We aim to determine the amount of content shared about cats over the total amount of data shared. We are aware that we are unable to scrape the whole content shared so we'll sample the content to a given amount per hour. To estimate the amount of cat's content we will make searches with cat's relevant keywords. We will classify pictures as belonging or not to the "cats" class based on image classification algorithms. We will then generate statistics on the amount of cats content versus overall content shared per hour per region (where the location is the country where the tweet was posted). We will share the code in open source. In addition, we will share the aggregated results. The aggregated results will be shared in the form of the amount of cat content per country per hour. We will compare this to the total amount of sampled data shared in twitter for the same hour and country.

In folder [ref](https://gitlab.com/agmarie1315/cats-internet/-/tree/main/ref) you can see some examples of researches that can be the basis of our study.
## Members
**Intern Student:** Mariia AGEEVA, 1st year of Master's Degree, University of Strasbourg

**Professor:** Cristel Pelsser, University of Strasbourg,
Publications:
- https://scholar.google.com/citations?user=H8FD7qQAAAAJ&hl=en
- https://clarinet.u-strasbg.fr/~pelsser/
